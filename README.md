# Kirby html-field
A nicely colored HTML/CSS/JS Field for Kirby V3

![screenshot](https://bitbucket.org/vitamin2/html-field/raw/cef5670308aadf0ec447d6b0cd6260940ff4d642/preview.png)

## Usage

As any Kirby field:

```yaml
fields:
  coder:
    label: SOME CODE
    type: html
```

You can deactivate the line numbers in the field
```yaml
        lineNumbers: false
```

## Installation
To install the plugin, please put it in the `site/plugins` directory.  
The field folder must be named `html-field`.

## Development

Follow step in the Kirby Docs.
https://getkirby.com/docs/guide/plugins/plugin-setup-panel

1. npm install -g parcel-bundler
2. yarn global add parcel-bundler
3. npm run dev 
4. npm run build

## License

MIT

Please visit our website if you like this Plugin [Vitamin2 AG](https://vitamin2.ch)

## Credits
[Vue Prism Code Editor](https://prism-editor.netlify.app/)