<?php

Kirby::plugin('vitamin2/html-field', [
    'fields' => [
        'html' => [
            'props' => [
                'lineNumbers' => function (bool $lineNumbers = TRUE) {
                    return $lineNumbers;
                },
            ]
        ]
            ],
    'fieldMethods' => [
        'toCode'=> function ($field) {
            return "<pre><code>" . rtrim(ltrim($field->html())) . "</code></pre>";
        }
    ]
]);